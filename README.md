# nixos_config

Exemple de configuration NixOS pour les cours :

- Programmation Fonctionnelle
- Calcul Haute Performance
- Prog. Fonctionnelle pour le Web

[Voir la page de cours.](https://juliendehos.gitlab.io)

[Voir l'introduction à NixOS.](https://juliendehos.gitlab.io/posts/env/index.html)


## configuration système

- éditer le fichier `/etc/nixos/configuration.nix` en root/sudo (voir le fichier [configuration.nix](configuration.nix) proposé, également téléchargeable : https://juliendehos.gitlab.io/nixos_config/configuration.nix)

- mettre à jour le système avec `sudo nixos-rebuild switch` 


## configuration utilisateur

- initialement, copier les fichiers proposés : `cp -r .config ~/`

- éditer le fichier `~/.config/nixpkgs/config.nix`

- mettre à jour avec `nix-env -iA nixos.myPackages`


## configuration de projets

- voir le dossier [exemples](exemples) et les explications sur la 
[page de cours](https://juliendehos.gitlab.io/posts/env/post35-nixos-dev.html).

