{ config, pkgs, ... }: {

  imports = [ ./hardware-configuration.nix ];

  nix.binaryCaches = [ 
    "https://cache.nixos.org/"
    #"https://nixcache.reflex-frp.org"  # haskell reflex
    #"https://hydra.dmj.io"             # haskell miso
  ];

  #nix.binaryCachePublicKeys = [ 
  #  "ryantrinkle.com-1:JJiAKaRv9mWgpVAz8dwewnZe0AzzEAzPkagE9SP5NWI="  # haskell reflex
  #];

  boot.kernelModules = [ "fuse" ];

  boot.loader.grub = {
    device = "/dev/sda"; 
    enable = true;
    version = 2;
  };

  #environment.shellInit = ''
  #  export http_proxy="http://TODO:TODO@192.168.22.62:3128" 
  #  export https_proxy="$http_proxy"
  #'';

  environment.systemPackages = with pkgs; [
    firefox 
    git 
    htop
    sshfs-fuse 
    sudo 
    tmux
    tree
    vim 
  ];

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "fr";
    defaultLocale = "fr_FR.UTF-8";
  };

  networking.proxy.default = "TODO:TODO@192.168.22.62:3128"; 
  networking.proxy.noProxy = "127.0.0.1,localhost,192.168.56.*"; 

  networking.hostName = "TODO"; 

  programs.bash.enableCompletion = true;

  services = {
    dbus.enable = true;
    udisks2.enable = true;

    postgresql = {
      enable = true;
      authentication = ''
        local all all trust
      '';
    };

    xserver = {
      desktopManager.xfce.enable = true;
      displayManager.lightdm.enable = true;
      enable = true;
      layout = "fr";
      # videoDrivers = [ "nvidia" ];
    };
  };
  # nixpkgs.config.allowUnfree = true;

  system.stateVersion = "18.03";

  time.timeZone = "Europe/Paris";

  users.extraUsers.TODO = {
    extraGroups = [ "wheel" "vboxusers" "docker" ];
    isNormalUser = true;
  };

  virtualisation.virtualbox.host.enable = true;
  virtualisation.docker.enable = true;
}

