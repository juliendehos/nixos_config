import System.Random

main :: IO ()
main = do
   gen <- newStdGen
   let ns = randoms gen :: [Double]
   mapM_ print (take 10 ns)

