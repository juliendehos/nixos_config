{ pkgs ? import <nixpkgs> {} }:
let 
  drv = pkgs.haskellPackages.callCabal2nix "hs_randmat1" ./. {};
in
if pkgs.lib.inNixShell then drv.env else drv

