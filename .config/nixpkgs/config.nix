with (import <nixos> {});

let

  _vim = import ./vim.nix { inherit pkgs; };

  _hie = (import (fetchTarball "https://github.com/domenkozar/hie-nix/tarball/master") {}).hies;

in

{

  allowUnfree = true;

  packageOverrides = pkgs: with pkgs; {

    myPython = python3.withPackages (ps: with ps; [
      numpy
    ]);

    myVscode = pkgs.buildEnv {
      name = "myVscode";
      paths = [
        vscode
        _hie
      ];
    };

    myVim = pkgs.buildEnv {
      name = "myVim";
      paths = [
        _vim
        ctags
      ];
    };

    myPackages = pkgs.buildEnv {
      name = "myPackages";
      paths = [
        cabal-install
        chromium
        cppcheck

        evince

        flow

        geany
        ghc
        gimp
        gnome3.eog
        gnumake

        hlint

        imagemagick

        libreoffice

        man-pages
        meld

        nixops

        octave

        proselint
        python3
        python3Packages.flake8
        python3Packages.glances

        qtcreator

        shellcheck
        sqlite
        sqlitebrowser

        tokei
      ];
    };
  };
}

